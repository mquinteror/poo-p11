package unam.fi.poo.hilos;

public class Tienda {
	
    public static void main(String[] args){

        Cliente client1 = new Cliente("Cliente 1", new int[]{1,2,3,3,4,4,7});
        Cliente client2 = new Cliente("Cliente 2", new int[]{20,1,3});
        Cliente client3 = new Cliente("Cliente 3", new int[]{30,1,3,5,8,9,10});
        
        long tiempoInicial = System.currentTimeMillis();
        Cajero cajero1 = new Cajero("Cajero 1", client3, tiempoInicial);
        Cajero cajero2 = new Cajero("Cajero 2", client2, tiempoInicial);

	cajero1.run();
	cajero2.run();
    }
}
