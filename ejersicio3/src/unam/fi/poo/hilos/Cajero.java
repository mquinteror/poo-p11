package unam.fi.poo.hilos;
// no hace faltra poq theread es de java land

public class Cajero implements Runnabe {

    private String nombre;
    private Cliente cliente;
    private long tiempoInicial;
	
    public Cajero(String nombre){
	this.nombre = nombre;
    }
    // sobrecargando el constructor
    public Cajero( String nombre, Cliente cliente, long tiempoInicial ){
	this.nombre = nombre;
	this.cliente = cliente;
	this.tiempoInicial = tiempoInicial;
    }
	
    public void run(){
	sop(this.nombre,"Comienza a cobrar al cliente:"+cliente.getNombre()+" en el tiempo:"+(System.currentTimeMillis()-tiempoInicial)/1000 +" segundos");
		
	for(int i=0;i<cliente.getCarrito().length;i++){
	    System.out.println("Procesando Articulo "+(i+1) );
	    this.procesaArticulo(cliente.getCarrito()[i]);
	    sop(this.nombre,"Procesando Articulo "+(i+1)+" tiempo("+(System.currentTimeMillis()-tiempoInicial)/1000 +") segundos" );
	}	
	sop(this.nombre,"termina el proceso de cobro en el tiempo:"+(System.currentTimeMillis()-tiempoInicial)/1000 +" segundos");
    }
	
    public void procesaArticulo(int tiempoArticulo){
	try {
	    Thread.sleep(tiempoArticulo*1000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public void sop(String cajero,String mensaje){
	System.out.println("Proceso("+cajero+")>>Mensaje:"+mensaje);
    }
}

